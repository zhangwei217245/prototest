/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vv.prototest.basics;

/**
 *
 * @author x-spirit
 */
public class StringTest {
    
    public static void main(String[] args) {
        String in = "abc";
        changeString(in);
        System.out.println(in);
    }
    
    public static String changeString(String in) {
        
        in += "d";
        
        return in;
    }
    
// ******   an explicit error may occur during compling phase
// ******   due to the unchangable variable "in" which was modified by "final"
//
//    public static String changeString(final String in) {
//        
//        in += "d";
//        
//        return in;
//    }
}
