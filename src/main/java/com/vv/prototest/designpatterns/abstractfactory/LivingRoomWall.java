package com.vv.prototest.designpatterns.abstractfactory;

/*
 * A concrete Wall for Living Room
 */
public class LivingRoomWall  extends Wall {
    private String wallName;
    public LivingRoomWall() {
        wallName = "LivingRoomWall";
    }
    public String getName() {
        return wallName;
    }
}