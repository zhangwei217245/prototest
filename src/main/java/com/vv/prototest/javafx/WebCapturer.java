package com.vv.prototest.javafx;

import javafx.animation.PauseTransition;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.Double;import java.lang.Exception;import java.lang.Override;import java.lang.String;import java.lang.System;import java.util.Map;

/**
 * Created by zhangwei on 14-4-29.
 *
 * @author zhangwei
 */
public class WebCapturer extends Application{

    private WebView webView;

    public static void capture(String url, double width, double height, String filePath) {
        Application.launch(WebCapturer.class, "--url="+url, "--width="+width, "--height="+height, "--filePath="+filePath);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Map<String, String> params = this.getParameters().getNamed();
        final String url = params.get("url");
        final double width = Double.valueOf(params.get("width"));
        final double height = Double.valueOf(params.get("height"));
        final File capturedImg = new File(params.get("filePath"));

        webView = new WebView();



        final PauseTransition pt = new PauseTransition();
        pt.setDuration(Duration.millis(500));
        pt.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                SnapshotParameters sp = new SnapshotParameters();

                WritableImage image = webView.snapshot(sp, null);
                BufferedImage bufferedImage = SwingFXUtils.fromFXImage(image, null);
                try {
                    ImageIO.write(bufferedImage, "png", capturedImg);

                    java.lang.System.out.println("Captured WebView to: " + capturedImg.getAbsoluteFile());
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    System.exit(0);
                }
            }
        });

        webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<Worker.State>() {
            @Override
            public void changed(ObservableValue<? extends Worker.State> observableValue, Worker.State state, Worker.State state2) {
                if (state2 == Worker.State.SUCCEEDED){
                    webView.setPrefSize(width, height);
                    pt.play();
                }
            }
        });

        ScrollPane webViewScroll = new ScrollPane();
        webViewScroll.setContent(webView);
        webViewScroll.setPrefSize(800, 300);

        VBox layout = new VBox(10);
        layout.setStyle("-fx-padding: 10; -fx-background-color: cornsilk;");
        layout.getChildren().setAll(
                webViewScroll
        );

        stage.setScene(new Scene(layout));
        stage.show();
        webView.getEngine().load(url.startsWith("http://") ? url : "http://" + url);







    }
}
