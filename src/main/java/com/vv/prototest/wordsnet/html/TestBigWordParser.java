/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vv.prototest.wordsnet.html;

import com.vv.prototest.wordsnet.Parser;
import com.vv.prototest.wordsnet.word.Word;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import jodd.io.FileUtil;
import jodd.io.NetUtil;
import jodd.jerry.Jerry;
import jodd.jerry.JerryFunction;
import jodd.util.SystemUtil;

/**
 *
 * @author x-spirit
 */
public class TestBigWordParser extends Parser {

    private static final String TEST_BIG_URI = "http://testbig.com/dictionary/search/";

    @Override
    public Word parse() {
        final Word word = new Word(this.getArg());
        try {
            // download the page super-efficiently
            File file = new File(SystemUtil.getTempDir(), word.getWord() + ".html");
            NetUtil.downloadFile(TEST_BIG_URI + word.getWord(), file);

            // create Jerry, i.e. document context
            Jerry docroot = Jerry.jerry(FileUtil.readString(file));

            // parse senses
            this.parseSenses(docroot, word);

            // parse prefix, root, suffix
        } catch (IOException ex) {
            Logger.getLogger(TestBigWordParser.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            // return result
            return word;
        }
    }

    private void parseSenses(final Jerry docroot, final Word word) {
        final AtomicInteger pos_index = new AtomicInteger();
        final AtomicInteger sense_index = new AtomicInteger();
        final StringBuffer sb_synonyms = new StringBuffer();
        final StringBuffer sb_gloss = new StringBuffer();
        docroot.$("div#page div.wrapper div#primary.short div.singlepage div.messages div table:eq(0) tr")
                .each(new JerryFunction() {

                    @Override
                    public boolean onNode(Jerry $this, int index) {
                        try {
//                            System.out.println("---");
//                            System.out.println($this.$("td").html());
                            // parse each row
                            Jerry $td = $this.$("td");

                            List<Word.MeaningPos> poses = word.getMeaningPos();
                            if ($td.attr("class").equals("meaningPos")) {// parse part of speech
                                word.addNewMeaningPos(pos_index.incrementAndGet(), $td.find(".meaningPosColor").text());
                                sense_index.set(0);
                            } else if ($td.attr("class").equals("meaningSense")) {// parse senses

                                if (poses.size() == pos_index.get()) {
                                    sb_synonyms.append($td.find(".meaningWordColor").html());
                                }

                            } else if ($td.attr("class").equals("meaningGloss")) {// parse gloss and samples
                                if (poses.size() == pos_index.get()) {
                                    sb_gloss.append($td.find(".meaningGlossColor").html()
                                            .replace("<font color=\"red\">", "").replace("</font>", ""));
                                }
                            }

                            if (sb_synonyms.length() > 0 && sb_gloss.length() > 0) {// composite synomyms and gloss
                                poses.get(poses.size() - 1)
                                .addMeaningSense(sense_index.incrementAndGet(),
                                        sb_synonyms.indexOf("(") >= 0 ? 
                                                sb_synonyms.delete(sb_synonyms.indexOf("("), sb_synonyms.indexOf(")") + 1).toString().trim() :
                                                sb_synonyms.toString().trim(),
                                        sb_gloss.toString().split("\"")[0],
                                        sb_gloss.toString().contains("\"")?
                                                sb_gloss.toString().split("\"")[1].replace("\"", "") :
                                                null);
                                sb_synonyms.delete(0, sb_synonyms.length());
                                sb_gloss.delete(0, sb_gloss.length());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        return true;
                    }

                });
    }
}
