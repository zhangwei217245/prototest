/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vv.prototest.wordsnet;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author x-spirit
 */
public abstract class Parser<T> {
    
    private String arg;
    
    private Map<Object, Object> argsMap = new HashMap<Object, Object>();
    
    public abstract T parse();

    /**
     * @return the arg
     */
    public String getArg() {
        return arg;
    }

    /**
     * @param arg the arg to set
     */
    public void setArg(String arg) {
        this.arg = arg;
    }

    /**
     * @return the argMap
     */
    public Map<Object, Object> getArgsMap() {
        return argsMap;
    }

    /**
     * @param argsMap the argMap to set
     */
    public void setArgsMap(Map<Object, Object> argsMap) {
        this.argsMap = argsMap;
    }
    
    
}
