/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.vv.prototest.wordsnet.word;

import java.util.ArrayList;

/**
 *
 * @author x-spirit
 */
public class Word {
    
    private String word;
    
    private ArrayList<MeaningPos> meaningPos = new ArrayList<MeaningPos>();

    public Word(String word) {
        this.word = word;
    }
    
    public MeaningPos addNewMeaningPos(int index, String posName) {
        MeaningPos mp = new MeaningPos(index, posName);
        this.meaningPos.add(mp);
        return mp;
    }
    
    /**
     * @return the meaningPos
     */
    public ArrayList<MeaningPos> getMeaningPos() {
        return meaningPos;
    }

    /**
     * @return the word
     */
    public String getWord() {
        return word;
    }

    /**
     * @param word the word to set
     */
    public void setWord(String word) {
        this.word = word;
    }
    
    
    
    public class MeaningPos{
        private int pos_index;
        private String pos;
        private ArrayList<MeaningSense> senses = new ArrayList<MeaningSense>();

        private MeaningPos(int pos_index, String pos) {
            this.pos_index = pos_index;
            this.pos = pos;
        }
        
        public MeaningSense addMeaningSense(int sense_index, String synonyms, String gloss, String sample) {
            MeaningSense ms = new MeaningSense(sense_index, synonyms, gloss, sample);
            senses.add(ms);
            return ms;
        }
        
        /**
         * @return the pos_index
         */
        public int getPos_index() {
            return pos_index;
        }

        /**
         * @param pos_index the pos_index to set
         */
        public void setPos_index(int pos_index) {
            this.pos_index = pos_index;
        }

        /**
         * @return the pos
         */
        public String getPos() {
            return pos;
        }

        /**
         * @param pos the pos to set
         */
        public void setPos(String pos) {
            this.pos = pos;
        }

        /**
         * @return the senses
         */
        public ArrayList getSenses() {
            return senses;
        }


        @Override
        public String toString() {
            return "MeaningPos{" + "pos_index=" + pos_index + ", pos=" + pos + ", senses=" + senses + '}';
        }
        
        
    }
    
    public class MeaningSense{
        private int sense_index;
        private String synonyms;
        private String gloss;
        private String sample;

        private MeaningSense(int sense_index, String synonyms, String gloss, String sample) {
            this.sense_index = sense_index;
            this.synonyms = synonyms;
            this.gloss = gloss;
            this.sample = sample;
        }

        
        /**
         * @return the sense_index
         */
        public int getSense_index() {
            return sense_index;
        }

        /**
         * @param sense_index the sense_index to set
         */
        public void setSense_index(int sense_index) {
            this.sense_index = sense_index;
        }

        /**
         * @return the synonyms
         */
        public String getSynonyms() {
            return synonyms;
        }

        /**
         * @param synonyms the synonyms to set
         */
        public void setSynonyms(String synonyms) {
            this.synonyms = synonyms;
        }

        /**
         * @return the gloss
         */
        public String getGloss() {
            return gloss;
        }

        /**
         * @param gloss the gloss to set
         */
        public void setGloss(String gloss) {
            this.gloss = gloss;
        }

        /**
         * @return the sample
         */
        public String getSample() {
            return sample;
        }

        /**
         * @param sample the sample to set
         */
        public void setSample(String sample) {
            this.sample = sample;
        }

        @Override
        public String toString() {
            return "MeaningSense{" + "sense_index=" + sense_index + ", synonyms=" + synonyms + ", gloss=" + gloss + ", sample=" + sample + '}';
        }

    }

    @Override
    public String toString() {
        return "Word{" + "word=" + word + ", meaningPos=" + meaningPos + '}';
    }
    
}
