/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vv.prototest.algorithms._gcd;

/**
 *
 * @author zhangwei
 */
public class GCD {
    
    /**
     * This algorithm calculate the greatest common divisor for both non-negatives, 
     * either of them must be positive.
     * 
     * However, for two zeros, there are no greatest common divisor for them, since any infinitely large
     * integer is the divisor for zero. So, this algorithm does not correctly calculate the GCD for 2 zeros. Even if
     * it returns a zero as the final result, but according to the rule of arithmetic division, 
     * zero cannot be the divisor of any number.
     *  
     * So neither m = 0 or n = 0 is acceptable if we want this algorithm to make sense.
     * 
     * 
     * 
     * @param m
     * @param n
     * @return 
     */
    public int euclidGCD(int m, int n) {
        int r = 0;
        while (n != 0) {
            r = m % n;
            m = n;
            n = r;
        }
        return r;
    }
    
    public int bruteForceGCD(int m, int n) {
        
        int t = min(m,n);
        
        while (true) {
            if (m % t == 0 && n % t == 0) {
                return t;
            }
            t--;
        }
    }
    
    private int min(int m, int n) {
        if (m - n < 0) {
            return m;
        }
        return n;
    }
    
    public static void main(String[] args) {
        GCD gcd = new GCD();
        int m = -5;
        int n = -10;
        int eu = gcd.euclidGCD(m, n);
        System.out.println("eu=" + eu);
        int br = gcd.bruteForceGCD(m, n);
        System.out.println("br=" + br);
        
    }
    
}
