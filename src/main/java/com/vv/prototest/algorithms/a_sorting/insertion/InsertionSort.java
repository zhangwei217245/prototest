package com.vv.prototest.algorithms.a_sorting.insertion;

import com.google.common.base.Stopwatch;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhangwei on 12/23/14.
 */
public class InsertionSort {

    //private int[] A = new int[]{5, 4, 6, 2, 1, 3, 7, 9, 8, 0};

    private int[] A = new int[]{3, 44, 38, 5, 47, 15, 36, 26, 27, 2, 46, 4, 19, 50, 48};

    //private int[] A = new int[]{0,1,2,3,4,5,6,7,8,9};

    private int count = 0;
    private int inner_count = 0;

    public void insertionSort(int[] A) {
        for (int j = 1; j < A.length; j++) {
            int key = A[j];
            // insert A[j] into the sorted sequence A[1...j-1];
            int i = j - 1;

            inner_count = 0;
            // move any item before the first unsorted to the bottom
            // until there are no longer bigger items in the sorted part.
            while (i >= 0 && A[i] > key) {

                A[i + 1] = A[i];
                i = i - 1;
                // algorithm-irrelevant code
                inner_count++;
            }
            // insert the first unsorted item to where it should be.
            A[i + 1] = key;
            // algorithm-irrelevant code
            count += inner_count==0?1:j*inner_count;

            System.out.printf("j=%d,\ni=%d,\nA[%d]=%d,\nA[%d]=%d,\nA[%d]=%d,\nkey=%d,\nA=%s\n", j, i, j, A[j], i, i < 0 ? -1 : A[i], i + 1, A[i + 1], key, Arrays.toString(A));
        }
    }

    public int[] getA() {
        return A;
    }

    public int getCount() {
        return count;
    }

    public static void main(String[] args) {
        InsertionSort is = new InsertionSort();
        Stopwatch sw = Stopwatch.createStarted();
        is.insertionSort(is.getA());
        System.out.printf("Finally, after %d round, A=%s, in %d milliseconds\n", is.getCount(),Arrays.toString(is.getA()),sw.elapsed(TimeUnit.MILLISECONDS));
    }
}
