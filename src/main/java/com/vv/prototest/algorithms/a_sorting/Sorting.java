/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vv.prototest.algorithms.a_sorting;

/**
 *
 * @author zhangwei
 */
public interface Sorting {
    
    public static final int a = 5;
    
    public Comparable[] init();
    
    public void sort();
    
    public Comparable[] getResult();
    
}
