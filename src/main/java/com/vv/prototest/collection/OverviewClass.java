/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vv.prototest.collection;

import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

/**
 *
 * @author x-spirit
 * @param <K>
 * @param <V>
 */
public class OverviewClass<K, V> {

    // interfaces hierarchy
    static Collection a;
    static Set b;
    static SortedSet c;
    static List d;
    static Queue e;
    static Deque f;

    static Map g;
    static SortedMap h;

    public static void testHashMapInitial() {
        g = new HashMap(16, 0.75f);
        for (int i = 0; i < 12; i++) {
            g.put("key" + i, "val" + i);
            System.out.println(g.toString());
        }
        for (int i = 12; i < 16; i++) {
            g.put("key" + i, "val" + i);
            System.out.println(g.toString());
        }
        for (int i = 16; i < 20; i++) {
            g.put("key" + i, "val" + i);
            System.out.println(g.toString());
        }
    }

    public static void main(String[] args) {
        testHashMapInitial();
    }

}
