package com.vv.ai.waterjug;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangwei on 1/29/16.
 */
public class WaterJugPair {

    private WaterJugPair parent;
    private WaterJug leftJug;
    private WaterJug rightJug;
    private WaterJug bigJug;
    private WaterJug smallJug;
    private List<Action> actions = new ArrayList<>();

    public WaterJugPair() {
    }

    public WaterJugPair(int capacity_left, int capacity_right) {
        this.leftJug = new WaterJug(capacity_left);
        this.rightJug = new WaterJug(capacity_right);
        this.determineSmallAndBig(capacity_left, capacity_right);
    }

    public WaterJugPair(WaterJugPair parent) {
        this.parent = parent;
        this.leftJug = parent.getLeftJug().copy();
        this.rightJug = parent.getRightJug().copy();
        this.determineSmallAndBig(this.leftJug.getCapacity(), this.rightJug.getCapacity());
    }

    public static WaterJugPair copy(WaterJugPair p) {
        WaterJugPair newPair = new WaterJugPair(p);
        newPair.actions = new ArrayList<>(p.getActions());
        return newPair;
    }

    private void determineSmallAndBig(int capacity_left, int capacity_right) {
        if (capacity_left >= capacity_right) {
            this.bigJug = this.leftJug;
            this.smallJug = this.rightJug;
        } else {
            this.bigJug = this.rightJug;
            this.smallJug = this.leftJug;
        }
    }

    public WaterJug getRightJug() {
        return rightJug;
    }

    public WaterJug getLeftJug() {
        return leftJug;
    }

    public WaterJug getBigJug() {
        return bigJug;
    }

    public WaterJug getSmallJug() {
        return smallJug;
    }

    public int getAmount_of_big() {
        return this.bigJug.getAmount();
    }

    public void setAmount_of_big(int amount_of_b) {
        this.bigJug.setAmount(amount_of_b);
    }

    public int getAmount_of_small() {
        return this.smallJug.getAmount();
    }

    public void setAmount_of_small(int amount_of_s) {
        this.smallJug.setAmount(amount_of_s);
    }

    public int getCapacity_big() {
        return this.bigJug.getCapacity();
    }

    public int getCapacity_small() {
        return this.smallJug.getCapacity();
    }

    public boolean isBigEmpty() {
        return this.bigJug.getAmount() == 0;
    }

    public boolean isBigNotEmpty() {
        return this.bigJug.getAmount() > 0;
    }

    public boolean isSmallEmpty() {
        return this.smallJug.getAmount() == 0;
    }

    public boolean isSmallNotEmpty() {
        return this.smallJug.getAmount() > 0;
    }

    public boolean isBothEmpty() {
        return isBigEmpty() && isSmallEmpty();
    }

    public void setAmountOfLeftAndRight(int left, int right) {
        if (left > this.leftJug.getCapacity()) {
            throw new IllegalAccessError(String.format("The amount of the left jug, which is %s, " +
                    "should not exceed its capacity which is %s.", left, this.leftJug.getCapacity()));
        }
        if (right > this.rightJug.getCapacity()) {
            throw new IllegalAccessError(String.format("The amount of the right jug, which is %s, " +
                    "should not exceed its capacity which is %s.", right, this.rightJug.getCapacity()));
        }
        this.leftJug.setAmount(left);
        this.rightJug.setAmount(right);
    }

    public WaterJugPair getParent() {
        return parent;
    }

    public List<Action> getActions() {
        return actions;
    }

    public boolean amountMatched(WaterJugPair p) {
        return this.leftJug.getAmount() == p.getLeftJug().getAmount()
                && this.rightJug.getAmount() == p.getRightJug().getAmount();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("");
        actions.stream().forEach(a -> sb.append("\r\n\t\t" + a.toString() + ""));
        return "{\r\n" +
                "\tresult=\"Solution %s\"" +
                ",\r\n \tl=" + this.leftJug.getAmount() +
                ",\r\n \tr=" + this.rightJug.getAmount() +
                ",\r\n \tpath=\r\n\t[" + sb +
                "\r\n \t]\r\n}";
    }

    public class WaterJug {
        private int capacity;
        private int amount;

        public WaterJug(int capacity) {
            this.capacity = capacity;
        }

        private WaterJug(int capacity, int amount) {
            this.capacity = capacity;
            this.amount = amount;
        }

        public int getAmount() {
            return amount;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public int getCapacity() {
            return capacity;
        }

        public void setCapacity(int capacity) {
            this.capacity = capacity;
        }

        public WaterJug copy() {
            return new WaterJug(this.capacity, this.amount);
        }
    }
}
