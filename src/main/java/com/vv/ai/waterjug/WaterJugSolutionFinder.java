package com.vv.ai.waterjug;

import java.util.Arrays;

/**
 * Created by zhangwei on 1/29/16.
 *
 * WaterJug problem:
 *
 * Left jug and right jug.
 *
 * Four actions to take:
 *
 * 1. Empty bigger one solely.
 * 2. Empty smaller one solely.
 * 3. Pour smaller one into empty bigger one.
 * 4. Pour bigger one into empty smaller one.
 *
 * Find out a solution path which leads to (l', r') from (l, r),
 * or print out message if no solution can be found.
 *
 * For example, initially, we have left jug whose capacity is 5L,
 * and also right jug whose capacity is 2L.
 * At the beginning the left jug has 3L of water, but the right one 1L.
 *
 * After taking all possible actions, find out a state where left jug contains 1L of water while right jug is empty.
 *
 */
public class WaterJugSolutionFinder {


    public static boolean solutionFound = false;
    public static boolean isFindingAllSolutions = false;
    private static WaterJugPair destination;

    /**
     * &lt;p&gt;
     * Both Empty is the post condition where no action could be matched. So, by using filter, we don't have to
     * worry about this case. &lt;br/&gt;
     * &lt;p/&gt;
     * <p>
     * &lt;p&gt;
     * As for detecting whether it is repeatable or not, we just care about pour-small-to-big or pour-big-to-small case.
     * Suppose we have A0 -> A1 -> A2, for certain post condition A1, if the amount of generating post condition A2 is
     * matched with the amount of its parent A0, then, this is called repeatable.&lt;br/&gt;
     * Refer to Action.isRepeatable()
     * &lt;p/&gt;
     *
     * @param p
     */
    public static void findPossibleSolution(WaterJugPair p) {

        if (p.amountMatched(destination)) {
            solutionFound = true;
            System.out.println(String.format(p.toString(), "Found!"));
            return;
        }

        System.out.println(String.format(p.toString(), "Not Found Yet!"));

        Arrays.asList(Action.values()).stream()
                .filter(action -> ((!solutionFound) || isFindingAllSolutions) && action.preConditionMatched(p))
                .map(action -> action.leadToPostCondition(p))
                .forEach(postCondition -> findPossibleSolution(postCondition));
    }

    public static void main(String[] args) {

        long maximumStackSizeOfAThread = Long.MAX_VALUE;

        new Thread(null, () -> {
            try {
                WaterJugPair waterJugPair = new WaterJugPair(5, 2);

                destination = WaterJugPair.copy(waterJugPair);

                destination.setAmountOfLeftAndRight(1, 0);

                waterJugPair.setAmountOfLeftAndRight(5, 1);

                isFindingAllSolutions = false;// true;

                System.out.println(String.format("Capacity Settings : l=%s, r=%s\r\nInitial State : l=%s, r=%s\r\nFinal State : l=%s, r=%s\r\n",
                        waterJugPair.getLeftJug().getCapacity(),
                        waterJugPair.getRightJug().getCapacity(),
                        waterJugPair.getLeftJug().getAmount(),
                        waterJugPair.getRightJug().getAmount(),
                        destination.getLeftJug().getAmount(),
                        destination.getRightJug().getAmount())
                );
                System.out.println("Start working...\r\n");


                findPossibleSolution(waterJugPair);

            } catch (Throwable t) {
                if (t instanceof StackOverflowError) {
                    System.out.println("StackOverFlow Happened!");
                    return;
                }
                System.out.println(t.getMessage());
                System.exit(0);
            } finally {
                if (!solutionFound) {
                    System.out.println("Solution Cannot Be Found!");
                }
            }
        }, "Thread-" + System.currentTimeMillis() % 10, maximumStackSizeOfAThread).start();

    }
}
