package com.vv.ai.waterjug;

/**
 * Created by zhangwei on 1/29/16.
 */
public enum Action {

    EMPTY_BIG_JUG(1) {
        @Override
        public WaterJugPair leadToPostCondition(WaterJugPair p) {
            WaterJugPair copy = this.copyAsPostCondition(p);
            copy.getBigJug().setAmount(0);
            return copy;
        }

        @Override
        public boolean preConditionMatched(WaterJugPair p) {
            return p.isBigNotEmpty();
        }
    },
    EMPTY_SMALL_JUG(2) {
        @Override
        public WaterJugPair leadToPostCondition(WaterJugPair p) {
            WaterJugPair copy = this.copyAsPostCondition(p);
            copy.getSmallJug().setAmount(0);
            return copy;
        }

        @Override
        public boolean preConditionMatched(WaterJugPair p) {
            return p.isSmallNotEmpty();
        }
    },
    POUR_SMALL_INTO_BIG(3) {
        @Override
        public WaterJugPair leadToPostCondition(WaterJugPair p) {
            WaterJugPair copy = this.copyAsPostCondition(p);
            copy.getBigJug().setAmount(p.getSmallJug().getAmount());
            copy.getSmallJug().setAmount(0);
            return copy;
        }

        @Override
        public boolean preConditionMatched(WaterJugPair p) {
            boolean baseCondition = p.isSmallNotEmpty() && p.isBigEmpty();
            return (!isRepeatable(p)) && baseCondition;
        }
    },
    POUR_BIG_INTO_SMALL(4) {
        @Override
        public WaterJugPair leadToPostCondition(WaterJugPair p) {
            WaterJugPair copy = this.copyAsPostCondition(p);
            if (p.getBigJug().getAmount() >= p.getSmallJug().getCapacity()) {
                copy.getBigJug().setAmount(p.getBigJug().getAmount() - p.getSmallJug().getCapacity());
                copy.getSmallJug().setAmount(p.getSmallJug().getCapacity());
            } else if (p.getBigJug().getAmount() < p.getSmallJug().getCapacity()) {
                copy.getSmallJug().setAmount(p.getBigJug().getAmount());
                copy.getBigJug().setAmount(0);
            }
            return copy;
        }

        @Override
        public boolean preConditionMatched(WaterJugPair p) {
            boolean baseCondition = p.isBigNotEmpty() && p.isSmallEmpty();
            return (!isRepeatable(p)) && baseCondition;
        }
    };


    private int index;

    Action(int index) {
        this.index = index;
    }

    public abstract boolean preConditionMatched(WaterJugPair p);

    public abstract WaterJugPair leadToPostCondition(WaterJugPair p);

    public WaterJugPair copyAsPostCondition(WaterJugPair p) {
        WaterJugPair copy = WaterJugPair.copy(p);
        copy.getActions().add(this);
        return copy;
    }

    public boolean isRepeatable(WaterJugPair p) {
        if (p.getParent() == null) {
            return false;
        }
        WaterJugPair child = leadToPostCondition(p);
        if (child.amountMatched(p.getParent())) {
            return true;
        }
        return false;
    }

    public int getIndex() {
        return index;
    }

    @Override
    public String toString() {
        return String.format("%s. %s", index, name());
    }
}
