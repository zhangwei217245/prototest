This is a project demostrating all fundamental knowledge in Java Programming, including the following fields of study:

<p style="text-shadow: 5px 5px 5px #FF0000;">algorithms,</p>
<p style="text-shadow: 5px 5px 5px #FF0000;">concurrency,</p>
<p style="text-shadow: 5px 5px 5px #FF0000;">design patterns,</p>
<p style="text-shadow: 5px 5px 5px #FF0000;">Java basic,</p>
<p style="text-shadow: 5px 5px 5px #FF0000;">Java advanced,</p>
<p style="text-shadow: 5px 5px 5px #FF0000;">and other features.</p>

<b style="text-shadow: 5px 5px 5px #FF0000;">新年好</b>